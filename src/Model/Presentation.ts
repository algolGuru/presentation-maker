type Presentation = {
	name: string,
	date: Date,
	author: string,
	sliderList: Array<Slide>
};
