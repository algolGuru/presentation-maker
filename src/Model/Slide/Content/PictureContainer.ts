type PictureContainer = Content & {
	width: number,
	height: number,
	image: Image
}
