type RichText = {
	color: string,
	font: Font,
	value: string,
}
