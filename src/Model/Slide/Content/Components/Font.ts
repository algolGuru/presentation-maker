type Font = {
	size: number,
	weight: number,
	family: string,
	lineHeight: number,
	style: string,
}
