type TextContainer = Content & {
	width: string,
	background: string,
	richText: RichText,
}
