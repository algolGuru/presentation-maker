type Slide = {
	background: string,
	animation: number,
	contentList: {
		[key: string]: Content
	}
}
